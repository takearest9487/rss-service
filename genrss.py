#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime

from lxml import html

from rfeed import Feed
from rfeed import Guid
from rfeed import Item
import requests


def mzitu_imgs(iid):
    url = "http://adr.meizitu.net/wp-json/wp/v2/i?id={}".format(int(iid))
    r = requests.get(url=url)
    content = r.json().get("content", "")
    imgs = ""
    for img in content.replace("\"", "").split(","):
        imgs += '<img src="{}"><br/>'.format(img)
    return imgs


def mzitu_date(iid):
    url = "https://m.mzitu.com/{}".format(int(iid))
    headers = {
        "User-Agent": "Mozilla/5.0"
    }
    r = requests.get(url=url, headers=headers)
    root = html.fromstring(r.text)
    pdate = root.xpath('//span[@class="time"]')[0].text
    rtn = datetime.strptime(pdate, "%Y-%m-%d %H:%M")
    return rtn


def mzitu():
    r = requests.get(
        url="http://adr.meizitu.net/wp-json/wp/v2/posts")
    items = []
    for entry in r.json():
        link = "https://m.mzitu.com/{}".format(entry["id"])
        items.append(Item(
            title=entry["title"],
            link=link,
            description=mzitu_imgs(entry["id"]),
            guid=Guid(link),
            pubDate=mzitu_date(entry["id"])))

    feed = Feed(
        title="Mzitu.com",
        link="https://takearest9487.gitlab.io/rss-service/mzitu.xml",
        description="RSS for mzitu.com",
        language="zh-CN",
        lastBuildDate=datetime.now(),
        items=items)

    with open("mzitu.xml", "w") as f:
        f.write(feed.rss())


if __name__ == "__main__":
    mzitu()
